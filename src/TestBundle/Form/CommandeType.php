<?php
// src/testBundle/Form/CommandeType.php

namespace TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CommandeType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('marketplace', 'text')
		->add('title', 'text')
		->add('orderAmount', 'text')
		->add('deliveryAddres','text')
		->add('save', 'submit')
		;
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'TestBundle\Entity\Commande'
			));
	}

	public function getName()
	{
		return 'oc_platformbundle_advert';
	}
}