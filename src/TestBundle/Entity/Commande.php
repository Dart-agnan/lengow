<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;
/**
 * Commande
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Commande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="marketplace", type="string", length=255)
     */
    private $marketplace;


    /**
     * @var string
     *
     * @ORM\Column(name="order_amount", type="decimal")
     */
    private $orderAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_addres", type="string", length=255)
     */
    private $deliveryAddres;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return Commande
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set orderPurchaseDate
     *
     * @param \DateTime $orderPurchaseDate
     * @return Commande
     */
    public function setOrderPurchaseDate($orderPurchaseDate)
    {
        $this->orderPurchaseDate = $orderPurchaseDate;

        return $this;
    }

    /**
     * Get orderPurchaseDate
     *
     * @return \DateTime 
     */
    public function getOrderPurchaseDate()
    {
        return $this->orderPurchaseDate;
    }

    /**
     * Set orderAmount
     *
     * @param string $orderAmount
     * @return Commande
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return string 
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * Set deliveryAddres
     *
     * @param string $deliveryAddres
     * @return Commande
     */
    public function setDeliveryAddres($deliveryAddres)
    {
        $this->deliveryAddres = $deliveryAddres;

        return $this;
    }

    /**
     * Get deliveryAddres
     *
     * @return string 
     */
    public function getDeliveryAddres()
    {
        return $this->deliveryAddres;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Commande
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
}
