<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TestBundle\Entity\Commande;
use DateTime;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\HttpFoundation\Request;
use TestBundle\Form\CommandeType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Yaml\Yaml;

class TestController extends Controller
{
	public function indexAction()
	{
		return $this->render('TestBundle:Default:index.html.twig');
	}

	public function urlAction()
	{
		$url= $this->container->getParameter('url_orders');
		$xml = simplexml_load_file($url) or die("feed not loading");
		foreach ($xml as $orders) {
			$i=0;
			foreach ($orders as $order) {
				if($order->marketplace!= ""){

					$marketplace =(string)  $order->marketplace;
					$orderAmount = (float) $order->order_amount;
					$deliveryAddres =(string)   $order->delivery_address->delivery_full_address;
					$title =(string)   $order->cart->products->product->title;

					// Création de l'entité Commande
					$commande = new Commande();
					$commande->setMarketplace($marketplace);
					$commande->setOrderAmount($orderAmount);
					$commande->setDeliveryAddres($deliveryAddres);
					$commande->setTitle($title);


					print_r("order n° ".$i." ajouté à la base");
					print_r("<< ".$marketplace."<br>");
					print_r("<br>"."<< ".$orderAmount."<br>");
					print_r("<< ".$deliveryAddres."<br>");
					print_r("<< ".$title."<br>");
					print_r("<br>");

					 // On récupère l'EntityManager
					$em = $this->getDoctrine()->getManager();

			    	// Étape 1 : On « persiste » l'entité
					$em->persist($commande);

					// Étape 2 : On « flush » tout ce qui a été persisté avant
					$em->flush();

				}
			}
		}


		// $orders = $xml->Orders->Order[1];
		return $this->redirectToRoute('test_grid', array(
			'xml' => $xml
			), 301);

	}

	public function myGridAction()
	{
		// Creates a simple grid based on your entity (ORM)
		$source = new Entity('TestBundle:Commande');

		// Get a Grid instance
		$grid = $this->get('grid');

		// Attach the source to the grid
		$grid->setSource($source);

		// Return the response of the grid to the template
		return $grid->getGridResponse('TestBundle:Test:test.html.twig');
	}

	public function addAction(Request $request)
	{
    // On crée un objet Commande

		$commande = new Commande();
		  // On crée le FormBuilder grâce au service form factory
		$form = $this->get('form.factory')->create(new CommandeType(), $commande);

		if ($form->handleRequest($request)->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($commande);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', 'Commande bien enregistrée.');

			return $this->redirectToRoute('test_grid', array(), 301);
		}

		return $this->render('TestBundle:Test:add.html.twig', array(
			'form' => $form->createView(),
			));
	}

	public function apiidAction($id, $yaml){
		$em = $this->getDoctrine()->getManager();

		if (null === $commande) {
			throw new NotFoundHttpException("La commande d'id ".$id." n'existe pas.");
		}
		$jsonEncoder = new JsonEncoder();        
		// Retourne bien les données
		echo("data <br>");
		print_r($commande);

		//Retourne du vide
		if($yaml){
			echo("<br>  <br> yaml".Yaml::dump($commande));

		}
		else{
			echo("<br>  <br> Json ".$jsonEncoder->encode($commande, $format = 'json'));
		}
		return $this->render('TestBundle:Default:index.html.twig', array());
	}

	public function apiAction($id, $format){
		$em = $this->getDoctrine()->getManager();
		$commande = $em->getRepository('TestBundle:Commande')->find($id);

		if (null === $commande) {
			throw new NotFoundHttpException("Aucune commandes");
		}

		$jsonEncoder = new JsonEncoder();        
		// Retourne bien les données
		echo("data <br>");
		print_r($commande);

		//Retourne du vide
		if($format == "yaml"){
			echo("<br>  <br> yaml ".Yaml::dump($commande));

		}
		else{
			echo("<br>  <br> Json ".$jsonEncoder->encode($commande, $format = 'json'));
		}

		return $this->render('TestBundle:Default:index.html.twig', array());
	}

	public function api2Action($format){
		$em = $this->getDoctrine()->getManager();

		$commande = $em->getRepository('TestBundle:Commande')->findAll();

		if (null === $commande) {
			throw new NotFoundHttpException("Aucune commandes");
		}

		$jsonEncoder = new JsonEncoder();        
		// Retourne bien les données
		echo("data <br>");
		print_r($commande);

		//Retourne du vide
		if($format == "yaml"){
			echo("<br>  <br> yaml ".Yaml::dump($commande));

		}
		else{
			echo("<br>  <br> Json ".$jsonEncoder->encode($commande, $format = 'json'));
		}

		return $this->render('TestBundle:Default:index.html.twig', array());
	}




}
