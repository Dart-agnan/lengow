# Test technique sf2

## Problématique :

Le but de ce test est de comprendre votre utilisation de Symfony 2. Il n'y a pas de piège, nous voulons simplement mesurer votre niveau technique. Le but est de récupérer des produits provenant d'un flux xml et de les manipuler. Vous pouvez utiliser le SGBD que vous souhaitez.

## Tâches :

Chaque tâche doit être faite avec les bonnes pratiques en utilisant les normes PSR.

	V 1. Créer un TestBundle. 
	V 2. Créer un fichier de configuration avec un paramètre url_orders : http://test.lengow.io/orders-test.xml.
	V 3. Créer une classe permettant d'aller récupérer le flux xml en utilisant le paramètre ci dessus.
	4. Faire que cette classe devienne un service "lengow_test" et que le paramètre "url_orders" et le "logger" soient passés en injection de dépendance. Logger doit être utilisé pour tracer les appels et les retours des téléchargements du fichier xml.
	V 5. Une fois le fichier récupérer, sauvegarder les commandes dans une BDD en utilisant une entité et doctrine. Vous n'êtes pas obligé de sauvegarder tous les champs, mais seulement 4 ou 5. Ne pas sauvegarder une commande si elle est déjà présente en base de donnée.
	V 6. Afficher les résultats sous forme de tableau en utilisant un bundle de gestion de grid : https://github.com/Abhoryo/APYDataGridBundle.
	V 7. Créer un formulaire sf2 pour insérer une commande en BDD.
	/ 8. Créer une route du type /api/ pour récupérer toutes les commandes en BDD au format "json".
	/ 9. Créer une route du type /api/%id_order%/ pour récupérer une commande sous format "json".
	/ 10. Offrir la possibilité avec un paramètre d'avoir les commandes au format "yaml".

